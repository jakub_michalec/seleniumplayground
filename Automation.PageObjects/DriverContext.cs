﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Automation.PageObjects
{
    public class DriverContext
    {
        public IWebDriver Driver { get; private set; }

        public void Start()
        {
            this.Driver = new ChromeDriver();
        }

        public void Stop()
        {
            this.Driver.Quit();
        }

        public void ChangeSizeOfWindow(int width, int height)
        {
            System.Drawing.Size windowSize = new System.Drawing.Size(width, height);
            this.Driver.Manage().Window.Size = windowSize;
        }
    }
}
