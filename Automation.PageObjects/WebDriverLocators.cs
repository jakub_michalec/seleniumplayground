﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automation.PageObjects
{
    public static class WebDriverLocators
    {
        private const int DefaultWaitTimeout = 15;

        public static IWebElement WaitAndFindElement(this IWebDriver driver, By by)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(DefaultWaitTimeout))
            {
                PollingInterval = TimeSpan.FromMilliseconds(100)
            };
            return wait.Until(drv => drv.FindElement(by));
        }

        public static bool ElementNotDisplayed(this IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElement(by).Displayed;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        public static bool ElementNotExists(this IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }
    }
}
