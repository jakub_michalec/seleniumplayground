﻿namespace Automation.PageObjects
{
    public class Strings
    {
        public static string FormNameKilgoreTrout = "Kilgore Trout";
    }

    public class Occupation
    {
        public static string Astronaut = "Astronaut";
        public static string Politician = "Politician";
        public static string ScienceFictionAuthor = "Science Fiction Author";
    }

    public class LinksElements
    {
        public static string ClickMe = "click me";
        public static string ClickThenWait = "click then wait";
        public static string ClickAfterWait = "click after wait";
    }

    public class RadioButtons
    {
        public static string WroteBook = "wrotebook";
        public static string DidNotWriteBook = "didntwritebook";
    }

    public class JsScripts
    {
        public static string RanThisJsFunction = "ran_this_js_function()";
        public static string GotReturnFromJsFunction = "return got_return_from_js_function()";
    }
}
