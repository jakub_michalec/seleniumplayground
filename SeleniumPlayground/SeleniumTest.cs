﻿using Automation.PageObjects;
using NUnit.Framework;

namespace SeleniumPlayground
{
    [TestFixture]
    public class SeleniumTest
    {
        private HomePage _homePage;
        private AnswerForm _asnwerForm;
        private TestForm _testForm;
        private BoxElements _boxElements;
        private Common _common;
        private const string StartingUrl = "http://timvroom.com/selenium/playground/";
        private DriverContext _driver;

        [SetUp]
        public void SetUp()
        {
            _driver = new DriverContext();
            _driver.Start();
            _driver.ChangeSizeOfWindow(850, 650);
        }

        [Test]
        public void SeleniumTestingTask()
        {
            _homePage = new HomePage(_driver);
            _homePage.OpenPage(StartingUrl);

            _asnwerForm = new AnswerForm(_driver) {TitleOfPage = _homePage.GetPageTitle()};

            _testForm = new TestForm(_driver) {TestFormName = Strings.FormNameKilgoreTrout};
            _testForm.SelectOccupation(Occupation.ScienceFictionAuthor);

            _boxElements = new BoxElements(_driver);
            var numberOfBlueBoxes = _boxElements.BlueBoxWebElement().Count.ToString();
            _asnwerForm.NumberOfBlueBoxes = numberOfBlueBoxes;

            _common = new Common(_driver);
            _common.ClickLinkElementByText(LinksElements.ClickMe);

            var classOfRedButton = _boxElements.RedBox().GetAttribute("class");

            _asnwerForm.RedBoxClassAttribute = classOfRedButton;

            _common.RunFunction(JsScripts.RanThisJsFunction);

            _asnwerForm.ValueReturnedFromJsFunction = _common.GetFuncValue(JsScripts.GotReturnFromJsFunction);

            _asnwerForm.RedBoxTextAnswer = _boxElements.RedBox().Text;

            _testForm.RadioButton(RadioButtons.WroteBook);

            _asnwerForm.WhichBoxIsOnTop = _boxElements.CompareBoxes();

            _asnwerForm.ElementWithIdIsVisible = _homePage.IsIshereVisible();

            _asnwerForm.PurpleBoxElementVisible = _boxElements.IsPurpleboxVisible();

            _common.ClickLinkElementByText(LinksElements.ClickThenWait);

            _common.ClickLinkElementByTextAndWait(LinksElements.ClickAfterWait);

            _homePage.AcceptAlertFromPage();

            _testForm.SubmitForm();

        }

        [TearDown]
        public void TearDown()
        {
            _driver.Stop();
        }
    }
}
