﻿using OpenQA.Selenium;

namespace Automation.PageObjects
{
    public class HomePage : BasePage
    {
        public HomePage(DriverContext driverContext) : base(driverContext) { }

        public HomePage OpenPage(string url)
        {
            this.Driver.Navigate().GoToUrl(url);
            return this;
        }

        public string GetPageTitle()
        {
            return this.Driver.Title;
        }

        public string IsIshereVisible()
        {
            return CheckIfElementExists("ishere");
        }

        private string CheckIfElementExists(string elementLocator)
        {
            var element = this.Driver.ElementNotExists(By.Id(elementLocator));
            return element ? "yes" : "no";
        }

        public void AcceptAlertFromPage()
        {
            var simpleAlert = this.Driver.SwitchTo().Alert();
            simpleAlert.Accept();
        }
    }
}
