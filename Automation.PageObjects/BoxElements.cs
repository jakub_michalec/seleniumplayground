﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace Automation.PageObjects
{
    public class BoxElements : BasePage
    {
        public BoxElements(DriverContext driverContext) : base(driverContext) { }

        public IReadOnlyCollection<IWebElement> BlueBoxWebElement()
        {
            return this.Driver.FindElements(By.ClassName("bluebox"));
        }

        public IWebElement RedBox()
        {
            return this.Driver.FindElement(By.Id("redbox"));
        }

        public IWebElement OrangeBox()
        {
            return this.Driver.FindElement(By.Id("orangebox"));
        }

        public IWebElement GreenBox()
        {
            return this.Driver.FindElement(By.Id("greenbox"));
        }

        public string CompareBoxes()
        {
            return GreenBox().Location.Y < OrangeBox().Location.Y ? "green" : "orange";
        }

        public string IsPurpleboxVisible()
        {
            return CheckIfElementIsVisible("purplebox");
        }

        private string CheckIfElementIsVisible(string elementLocator)
        {
            var element = this.Driver.ElementNotDisplayed(By.Id(elementLocator));
            return element ? "yes" : "no";
        }

    }
}
