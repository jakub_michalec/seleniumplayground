﻿using OpenQA.Selenium;

namespace Automation.PageObjects
{
    public class AnswerForm : BasePage
    {
        public AnswerForm(DriverContext driverContext) : base(driverContext) { }

        public string TitleOfPage
        {
            set
            {
                this.Driver.FindElement(By.Id("answer1")).SendKeys(value);
            }
        }

        public string NumberOfBlueBoxes
        {
            set
            {
                this.Driver.FindElement(By.Id("answer4")).SendKeys(value);
            }
        }

        public string RedBoxClassAttribute
        {
            set
            {
                this.Driver.FindElement(By.Id("answer6")).SendKeys(value);
            }
        }

        public string ValueReturnedFromJsFunction
        {
            set
            {
                this.Driver.FindElement(By.Id("answer8")).SendKeys(value);
            }
        }

        public string RedBoxTextAnswer
        {
            set
            {
                this.Driver.FindElement(By.Id("answer10")).SendKeys(value);
            }
        }

        public string WhichBoxIsOnTop
        {
            set
            {
                this.Driver.FindElement(By.Id("answer11")).SendKeys(value);
            }
        }

        public string ElementWithIdIsVisible
        {
            set
            {
                this.Driver.FindElement(By.Id("answer13")).SendKeys(value);
            }
        }

        public string PurpleBoxElementVisible
        {
            set
            {
                this.Driver.FindElement(By.Id("answer14")).SendKeys(value);
            }
        }
    }
}
