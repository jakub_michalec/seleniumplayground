﻿using OpenQA.Selenium;

namespace Automation.PageObjects
{
    public class Common : BasePage
    {
        public Common(DriverContext driverContext) : base(driverContext) { }

        public void ClickLinkElementByTextAndWait(string linkName)
        {
            this.Driver.WaitAndFindElement(By.LinkText(linkName)).Click();
        }

        public void RunFunction(string funcName)
        {
            ((IJavaScriptExecutor)this.Driver).ExecuteScript(funcName);
        }

        public string GetFuncValue(string funcName)
        {
            return ((IJavaScriptExecutor)this.Driver).ExecuteScript(funcName).ToString();
        }

        public void ClickLinkElementByText(string linkName)
        {
            this.Driver.FindElement(By.LinkText(linkName)).Click();
        }
    }
}
