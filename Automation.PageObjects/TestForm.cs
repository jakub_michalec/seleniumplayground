﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automation.PageObjects
{
    public class TestForm : BasePage
    {
        public TestForm(DriverContext driverContext) : base(driverContext) { }

        public string TestFormName
        {
            set
            {
                this.Driver.FindElement(By.Id("name")).SendKeys(value);
            }
        }

        public void SelectOccupation(string occupation)
        {
            var dropdownElement = new SelectElement(this.Driver.FindElement(By.Id("occupation")));
            dropdownElement.SelectByText(occupation);
        }

        public void SubmitForm()
        {
            var submitButton = this.Driver.FindElement(By.Id("submitbutton"));
            submitButton.Click();
        }

        public void RadioButton(string radioBtnValue)
        {
            var radioButton = this.Driver.FindElement(By.XPath($"//*[@value='{radioBtnValue}']"));

            if (!radioButton.Selected)
                radioButton.Click();
        }
    }
}
