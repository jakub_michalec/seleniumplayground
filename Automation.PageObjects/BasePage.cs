﻿using OpenQA.Selenium;

namespace Automation.PageObjects
{
    public class BasePage
    {
        public BasePage(DriverContext driverContext)
        {
            this.DriverContext = driverContext;
            this.Driver = driverContext.Driver;
        }

        protected IWebDriver Driver { get; set; }

        protected DriverContext DriverContext { get; set; }
    }
}
